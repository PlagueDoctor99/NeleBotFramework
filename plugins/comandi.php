<?php

/*
NeleBotFramework
	Copyright (C) 2018-1019  PHP-Coders

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

if ($cmd == 'start') {
    sm($chatID, bold("Bot avviato!") . "\nUsa /help per una lista di comandi.\nℹ️ " . code("Versione 2.6.3"));
    die;
}

if ($cmd == "tag") {
    sm($chatID, tag());
    die;
}

if ($cmd == 'cancel') {
    db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["", $userID]);
    sm($chatID, "Comando annullato.");
    die;
}

if ($cmd == 'documents') {
    $menu[] = [
        [
            "text" => "Da locale",
            "callback_data" => "doc_locale"
        ],
        [
            "text" => "Da link",
            "callback_data" => "doc_link"
        ],
        [
            "text" => "Da file_id",
            "callback_data" => "doc_file_id"
        ]
    ];
    sm($chatID, "Documenti", $menu);
    die;
}

if (strpos($cbdata, "doc_") === 0) {
    $tipo = str_replace('doc_', '', $cbdata);
    $documents = [
        'locale' => "plugins.json",
        'link' => "https://t.me/HostMediaMCTime/779",
        'file_id' => "BQADBAADVwYAAoxY0FAPKmX8PWlhBwI" // Ogni Bot ha i propri file ID
    ];
    $config['response'] = true;
    cb_reply($cbid, '', false);
    $r = sd($chatID, $documents[$tipo], "test");
    if (!$r['ok']) {
        sm($chatID, "Error: " . json_encode($r));
    }
    die;
}

if ($cmd == "type") {
    db_query("UPDATE utenti SET page = ? WHERE user_id = ?", ["testMessage", $userID]);
    sm($chatID, "OK, inviami un messaggio e ti dirò che tipo di messaggio è...");
    die;
}

if ($u['page'] == "testMessage" and $typechat == "private") {
    sm($chatID, "Tipo di messaggio: $messageType");
    die;
}

if ($cmd == 'buttons') {
    $menu[] = [
        [
            'text' => "Mostra",
            'callback_data' => 'mostra'
        ],
        [
            'text' => "Mostra URL",
            'callback_data' => 'mostraurl'
        ],
    ];
    $menu[] = [
        [
            'text' => "Inline",
            'switch_inline_query_current_chat' => 'messaggio'
        ],
        [
            'text' => "Condividi Inline",
            'switch_inline_query' => 'messaggio'
        ],
    ];
    $menu[] = [
        [
            'text' => "URL",
            'url' => 'https://t.me/' . $config['username_bot']
        ],
        [
            'text' => "Condividi URL",
            'url' => 'https://t.me/share/url?' . http_build_query([
                    "text" => "Testo da condividere",
                    "url" => "https://t.me/" . $config['username_bot']
                ])
        ],
    ];
    sm($chatID, "Bottoni", $menu);
    die;
}

if ($cbdata == 'mostra') {
    cb_reply($cbid, 'Ciao', true);
    die;
}

if ($cbdata == 'mostraurl') {
    cb_url($cbid, "https://t.me/" . $config['username_bot'] . "?start=Test");
    die;
}

if ($cmd == "start Test") {
    sm($chatID);
    die;
}

if ($cmd == 'help' and $typechat == "private") {
    $menu[] = [
        [
            'text' => "Funzioni Inline",
            'switch_inline_query_current_chat' => ''
        ],
    ];
    $menu[] = [
        [
            'text' => "Source Bot",
            'url' => 't.me/NelePHPFramework'
        ],
    ];
    sm($chatID, bold("Comandi del Bot") . "
/start - Avvia il Bot
/help - Ottieni la lista dei comandi
/ping - Ottieni la velocità del Bot
/jsondump - Ottieni un dump in json della tua update
/infome - Visualizza i tuoi dati
/reply - Risponde a te e ti chiede di rispondere
/buttons - Esempio di pulsanti inline
/html - Ottieni lo stile di formazione in HTML
/markdown - Ottieni lo stile di formattazione in Markdown
/type - Scopri il tipo di messaggio inviato
/propic - Ottieni i file_id della tua ultima foto
/animation - Invia una gif
/editPhoto - Modifica un media
/contact - Invia un contatto
/venue - Invia la posizione di un posto
/location - Invia una posizione in live
@" . $config['username_bot'], $menu);
    die;
}

if ($cmd == "reply") {
    sm($chatID, "Risposta. \nAdesso rispondi tu.", 'rispondimi', '', $msgID);
    die;
}

if ($cmd == 'jsondump') {
    sm($chatID, code(substr(json_encode($original_update, JSON_PRETTY_PRINT), 0, 4095)));
    die;
}

if (in_array($cmd, ['html', 'markdown'])) {
    $config['parse_mode'] = $cmd;
    $testo = textspecialchars(bold("Bold")) . " = " . bold("Bold") . "\n";
    $testo .= textspecialchars(italic("Italic")) . " = " . italic("Italic") . "\n";
    $testo .= textspecialchars(code("Fixed")) . " = " . code("Fixed") . "\n";
    $testo .= textspecialchars(text_link("Text Link", 'http://www.example.com')) . " = " . text_link("Text Link",
            'http://www.example.com') . "\n";
    sm($chatID, $testo);
    die;
}

if ($cmd == 'db' and $typechat === "channel") {
    sm($chatID, code(json_encode($c, JSON_PRETTY_PRINT)));
    die;
}

if ($cmd == 'infome') {
    sm($chatID,
        bold("Informazioni Utente Telegram") . "\nNome: " . code($nome) . "\nCognome: " . code($cognome) . "\nUsername: " . code($username) . "\nID: " . code($userID) . "\nLingua: " . code($lingua) . "\n\n" . bold("Informazioni Utente Database") . "\nNome: " . code($u['nome']) . "\nCognome: " . code($u['cognome']) . "\nUsername: " . code($u['username']) . "\nID: " . code($u['user_id']) . "\nLingua: " . code($u['lang']));
    die;
}

if ($cmd == 'propic') {
    $config['json_payload'] = false;
    $photos = getPropic($userID);
    if ($photos['ok']) {
        sp($chatID, $photos['result']['photos'][0][count($photos['result']['photos'][0]) - 1]['file_id'],
            "Hai " . count($photos['result']['photos']) . " foto profilo.");
    } else {
        sm($chatID, bold("Error " . $photos['error_code']) . "\n" . $photos['description']);
    }
    die;
}

if ($cmd == 'contact') {
    sc($chatID, "+13253080294", "Presidente", "VoIP");
    die;
}

if ($cmd == 'editPhoto') {
    $config['response'] = true;
    $menu[0] = [
        [
            "text" => "Cambia",
            "callback_data" => "cbEditPhoto"
        ],
    ];
    $mes = sp($chatID, "t.me/NeleB54GoldBlog", 'cia', $menu);
    die;
}

if ($cbdata == "cbEditPhoto") {
    $menu[] = [
        [
            "text" => "Fatto!",
            "url" => "t.me/NeleB54Gold"
        ]
    ];
    media_cb_reply($cbid, "Text", true, $cbmid, "t.me/NeleB54Gold", 'photo', "Ciao", $menu);
    die;
}

if ($cmd == 'animation') {
    sgif($chatID, "https://t.me/NeleB54GoldBlog/568", 'Rotola');
    die;
}

if ($cmd == 'venue') {
    sven($chatID, 41.90, 12.50, "Capitale d'Italia", 'Roma');
    die;
}

if ($cmd == 'location') {
    $menu[] = [
        [
            'text' => "Cambia Location",
            'callback_data' => '/editLocation 42.90 13.50'
        ],
    ];
    $menu[] = [
        [
            'text' => "Ferma Location",
            'callback_data' => '/stopLocation'
        ],
    ];
    sendLocation($chatID, 41.90, 12.50, $menu, 7200);
    die;
}

if (strpos($cbdata, '/editLocation') === 0) {
    $e = explode(' ', $cbdata, 3);
    $lati = $e[1];
    $long = $e[2];
    $latisin = $lati + 1;
    $latides = $lati - 1;
    $longsu = $long + 1;
    $longgiu = $long - 1;
    if ($longsu < 90 or $latides < 90) {
        $menu[] = [
            [
                'text' => "Cambia Location",
                'callback_data' => "/editLocation $longsu $latides"
            ],
        ];
    }
    $menu[] = [
        [
            'text' => "Ferma Location",
            'callback_data' => '/stopLocation'
        ],
    ];
    editLocation($chatID, $lati, $long, $cbmid, $menu);
    cb_reply($cbid);
    die;
}

if ($cbdata == '/stopLocation') {
    $menu[] = [
        [
            'text' => "Developer",
            'url' => "t.me/NeleB54Gold"
        ],
    ];
    cb_reply($cbid, 'Posizionamento terminato', true);
    stopLocation($chatID, $cbmid, $menu);
    die;
}
