# Versione: <code>2.6.3</code>

<b>Changelog:</b>

- ➕ <b>Aggiunta</b> la sezione <b>amministrazione</b> sulle <b>info utenti</b>!
- ➕ <b>Aggiunto</b> il <b>comando</b> <code>ping</code> per <b>redis</b> e per il <b>database</b>. <i>(Vedi la guida)</i>
- ➕ <b>Adesso il menu di gestione utenti/gruppi/canali si aggiorna in caso di modifiche ad uno di essi.</b>
- ➕ <b>Cliccando sul numero dell'u/g/c su gestione ti porta sulle info di esso.</b>
- ➕ <b>Aggiunta</b> la possibilità di <b>ricevere risultati diversi</b> da <code>db_query</code>. <i>(Vedi la guida)</i>
- ➕ <b>Aggiunta</b> la <b>funzione</b> <code>setStatus</code> <i>(se è attivo il database)</i> per <b>facilitare il set dello stato</b>.
- ➕ <b>Aggiunto</b> il <b>comando</b> <code>/getlasterror</code> per <b>prendere l'ultimo errore dal webhook</b>.
- 🔄 <b>Interfaccia del plugin manager aggiornata.</b>
- 🔄 <b>Aggiornati i messagi di aiuto.</b>
- ✅ <b>Fixato</b> la <b>ricerca</b> del <b>canale</b> per <b>username</b> da <code>/info</code>. 
- ✅ <b>Fixato ban dell'AntiFlood.</b>
- ✅ <b>Fixata</b> la <b>funzione</b> <code>promote</code>.
- ✅ <b>Fixata</b> la <b>funzione</b> <code>unpin</code>.
- ✅ <b>Fixato</b> la <b>segnalazione errori</b> da <code>db_query</code>.
- ✅ <b>Fixato</b> l'<b>errore Fatal</b> di <b>execute</b> con <b>SQLite</b>.
- ✅ <b>Migliorata la funzione di log NBF.</b>
- ✅ <b>Cambiamenti e bugfix minori.</b>

<b>⚠️ ATTENZIONE ⚠️</b><br>
<b>Lo schema precedente del database è incompatibile con questa versione, in caso di aggiornamento del framework, l'intero database dovrà essere ricreato!</b>

<b>Buon divertimento con il Framework!</b> <br/> © [PHP-Coders](https://gitlab.com/PHP-Coders)
