<p align="center"> 
    <img src="https://i.imgur.com/F4YcY2D.jpg" alt="Banner" /> 
</p>

## Cos'è NeleBot?
**NeleBot è il primo framework** per [Bot Telegram](https://core.telegram.org/bots) in [PHP](https://php.net) **completamente realizzato da 0** e da uno 
**sviluppatore** con **più di 2 anni** di **esperienza** con il **linguaggio stesso.**

## Perche dovrei usare NeleBot?
**NeleBot ha tante funzioni in più che renderanno la creazione del tuo bot più facile e sicura!**

Ad esempio:

* **Aggiunta di una password nel file config.php per autenticare le richieste**
* **Migliore gestione di tutte le richieste in entrata e in uscita (per il bot)**
* **Migliore gestione di utenti/gruppi/canali**
* **Ogni metodo supportato dalla BotAPI è stato inserito in functions.php, quindi tu preoccupati di scrivere il tuo bot e se serve una funzione apposita, richiamala semplicemente** <br/>_Se non sai come si chiama la funzione basta cercare il nome che riportato dalle docs della_ [BotAPI](https://core.telegram.org/bots/api) _e cercarlo nel file functions.php_
* **Possibilità di attivare/disattivare la developer mode**
* **Possibilità di attivare/disattivare la whitelist utenti**
* **Molto altro ancora da scoprire scaricando il framework**

## Requisiti di sistema
- VPS: (consigliamo [rhosting.it](https://app.rhosting.it/aff.php?aff=8))
* Distro linux
* NGINX (consigliato)
* Dominio con certificato SSL valido
* PHP7
* cURL
* MySQL/MariaDB (opzionale se si usa PostGreSQL)
* PostGreSQL (opzionale se si usa MySQL/MariaDB)
* Redis (opzionale, obbligatorio se si vuole utilizzare l'antiflood integrato)
* Git (opzionale)

-----------------
- WebHosting: (consigliamo [rhosting.it](https://app.rhosting.it/aff.php?aff=8))
* Dominio con certificato SSL valido
* PHP7
* MySQL/MariaDB (opzionale se si usa PostGreSQL)
* PostGreSQL (opzionale se si usa MySQL/MariaDB)

**Il funzionamento corretto con l'utilizzo di PHP5 non è garantito**

Installazione
---------------

### VPS
**Se usi un VPS ci sono 2 modi per scaricarlo:**
* Usando il comando `git clone https://gitlab.com/PHP-Coders/NeleBotFramework`
* Scaricando il file zip dal [repository gitlab](https://gitlab.com/PHP-Coders/NeleBotFramework) e ricaricandolo nella cartella sul VPS

### WebHost (non altervista)
* Scaricando il file zip dal [repository gitlab](https://gitlab.com/PHP-Coders/NeleBotFramework) e ricaricandolo in una cartella sul tuo webhost
 
## Canale Ufficiale
- [Clicca qui](https://t.me/NelePHPFramework)

## Guida Ufficiale
- [Clicca qui](https://telegra.ph/NeleBot--PHP-Framework-per-Bot-Telegram-07-20)

## Sorgenti del WebHook Installer
- [Clicca qui](https://gitlab.com/PHP-Coders/NeleBotWebHookInstaller)

## Gruppo di supporto
- Link 1 [Clicca qui](https://t.me/NeleFrameworkSupport)
- Link 2 [Clicca qui](https://t.me/NeleBotSupport)
 
© 2018-2019 [PHP-Coders](https://gitlab.com/PHP-Coders)